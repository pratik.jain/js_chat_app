function loadChannels() {
    fetch('http://localhost:5000/channels/')
        .then(
            function(result) {
                if (result.status == 200) {
                    var channels;
                    result.json().then(function(data) {
                        channels = data.resources;
                        console.log(channels);
                        for (channel of channels) {
                            var channel_name = channel.name;
                            var channel_id = channel.id;
                            newChannel = document.createElement("div");
                            newChannel.innerHTML = channel_name;
                            newChannel.id = channel_id;
                            document.getElementById("channels-name").appendChild(newChannel);
                            document.getElementById(channel_id).onclick = loadMessages;


                        }
                    })


                } else {
                    alert("Unable to fetch the Channel list.");
                }
            }
        )
}

function addChannel() {
    var channel_name = document.getElementById("input-add-channel").value;
    if (channel_name != '') {
        fetch('http://localhost:5000/channels/', {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: channel_name
                })

            })
            .then(result => result.json())
            .then(data => console.log(data))

    } else {
        alert("Please Enter Channel name.");
    }
    document.getElementById("input-add-channel").value = '';
    document.location.reload();
}

function loadMessages() {
    var chatArea = document.getElementById("chat-messages");
    while (chatArea.firstChild) {
        chatArea.removeChild(chatArea.firstChild);
    }
    var channel_name = event.target.innerHTML;
    var id = '#' + event.target.id;
    var chat_channel = document.getElementsByClassName("chat-channel-name");
    chat_channel[0].innerHTML = channel_name;
    chat_channel[0].id = id;
    query = 'http://localhost:5000/messages/?channel_id=' + event.target.id;
    fetch(query)
        .then(
            function(result) {
                if (result.status == 200) {
                    var channels;
                    result.json().then(function(data) {
                        messages = data.resources;
                        for (message of messages) {
                            var chat = message.text;
                            newChat = document.createElement("div");
                            newChat.innerHTML = chat;
                            document.getElementById("chat-messages").appendChild(newChat);


                        }
                    })


                } else {
                    alert("Unable to fetch the Channel list.");
                }
            }
        )
}

function sendMessage() {
    var chat_channel = document.getElementsByClassName("chat-channel-name")
    var chat_channel_id = chat_channel[0].id.slice(1);
    var chat_text = document.getElementById("user-input").value;
    if (chat_text != '') {
        fetch('http://localhost:5000/messages/', {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    channel_id: chat_channel_id,
                    text: chat_text,
                    username: "pratik"
                })

            })
            .then(result => result.json())
            .then(data => console.log(data))

    } else {
        alert("Please Type a message.");
    }
    document.getElementById("user-input").value = '';

    newChat = document.createElement("div");
    newChat.innerHTML = chat_text;
    document.getElementById("chat-messages").appendChild(newChat);
}